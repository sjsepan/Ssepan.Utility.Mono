﻿using System;
using System.Configuration;
using System.Reflection;

namespace Ssepan.Utility.Mono
{
    public static class Configuration
    {
        /// <summary>
        /// Generic method to read a value that must also be parsed
        /// </summary>
        /// <typeparam name="TStruct"></typeparam>
        /// <param name="connectionStringName"></param>
        /// <param name="connectionString"></param>
        /// <returns></returns>
        public static bool ReadValue<T>(string settingName, out T setting) where T: struct
        {
            bool returnValue = default(bool);
            setting = default(T);

            try
            {

                object settingObject = null;
                
                settingObject = ConfigurationManager.AppSettings[settingName];
                if (settingObject == null)
                {
                    throw new ApplicationException(string.Format("Configuration connectionString was not found: {0}", settingName));
                }
                if (settingObject.ToString() == string.Empty)
                {
                    throw new ApplicationException(string.Format("Configuration connectionString was empty: {0}", settingName));
                }
                //Note:unable to access TryParse() without reflection within generic class--SJS
                if (!Parsing.TryParseGeneric<T>(settingObject.ToString(), out setting))
                {
                    throw new ApplicationException(string.Format("Configuration connectionString '{0}' was incorrectly formatted: {1}", settingName, settingObject.ToString()));
                }

                returnValue = true;
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }
            return returnValue;
        }

        /// <summary>
        /// Method to read a string value that does not need to be parsed
        /// </summary>
        /// <param name="connectionStringName"></param>
        /// <param name="connectionString"></param>
        /// <returns></returns>
        public static bool ReadString(string settingName, out string setting)
        {
            bool returnValue = default(bool);
            setting = default(string);

            try
            {

                object settingObject = null;

                settingObject = ConfigurationManager.AppSettings[settingName];
                if (settingObject == null)
                {
                    throw new ApplicationException(string.Format("Configuration connectionString was not found: {0}\r\nThis condition may not be an error if the connectionString is optional.\r\nThis warning may be unrelated to subsequent messages.", settingName));
                }
                if (settingObject.ToString() == string.Empty)
                {
                    throw new ApplicationException(string.Format("Configuration connectionString was empty: {0}", settingName));
                }
                setting = settingObject.ToString();

                returnValue = true;
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }
            return returnValue;
        }

        /// <summary>
        /// Method to specifically read a connection string value
        /// </summary>
        /// <param name="connectionStringName"></param>
        /// <param name="connectionString"></param>
        /// <returns></returns>
        public static bool ReadConnectionString
        (
            string connectionStringName, 
            out string connectionString,
            out string providerName
        )
        {
            bool returnValue = default(bool);
            object settingObject = default(object);
            connectionString = default(string);
            providerName = default(string);

            try
            {
                settingObject = ConfigurationManager.ConnectionStrings[connectionStringName];
                if (settingObject == null)
                {
                    throw new ApplicationException(string.Format("Configuration connectionString (connection string) was not found: {0}", connectionStringName));
                }

                if (((ConnectionStringSettings)settingObject).ConnectionString == string.Empty)
                {
                    throw new ApplicationException(string.Format("Configuration connectionString (connection string) was empty: {0}", connectionStringName));
                }
                connectionString = ((ConnectionStringSettings)settingObject).ConnectionString;

                if (((ConnectionStringSettings)settingObject).ProviderName == string.Empty)
                {
                    throw new ApplicationException(string.Format("Configuration connectionString (provider name) was empty: {0}", connectionStringName));
                }
                providerName = ((ConnectionStringSettings)settingObject).ProviderName;

                returnValue = true;
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }
            return returnValue;
        }
    }
}

