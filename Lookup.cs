﻿//#define USEEVENTLOG

using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Ssepan.Utility.Mono
{
    public class Lookup
    {
        public Lookup()
        {
        }

        public Lookup
        (
            string value,
            string text
        ) :
            this()
        {
            Value = value;
            Text = text;
        }

        private string _Value = default(string);
        public string Value
        {
            get { return _Value; }
            set { _Value = value; }
        }

        private string _Text = default(string);
        public string Text
        {
            get { return _Text; }
            set { _Text = value; }
        }


        public static List<Lookup> GetEnumLookup<TEnum>(bool addNotSelectedItem = false)
        {
            List<Lookup> returnValue = default(List<Lookup>);

            try
            {
                returnValue = (from Enum value in Enum.GetValues(typeof(TEnum))
                               select new Lookup(value.ToString(), value.ToString())).ToList();
                if (addNotSelectedItem)
                {
                    returnValue.Insert(0, new Lookup(string.Empty, "(Not Selected)"));
                }
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }
            return returnValue;
        }
    }
}
