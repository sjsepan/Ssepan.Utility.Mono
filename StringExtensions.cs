﻿using System;

namespace Ssepan.Utility.Mono
{
    public static class StringExtensions
    {
        /// <summary>
        /// Perform string.Contains(value) with case-sensitivity explicitly on or off.
        /// </summary>
        /// <param name="source"></param>
        /// <param name="value"></param>
        /// <param name="caseInsensitive"></param>
        /// <returns></returns>
        public static bool Contains(this string source, string value, bool caseInsensitive)
        {

            if (caseInsensitive)
            {
                int results = source.IndexOf(value, StringComparison.CurrentCultureIgnoreCase);
                return results == -1 ? false : true;
            }
            else
            {
                return source.Contains(value);
            }

        }
    }
}
