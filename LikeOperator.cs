﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Reflection;

namespace Ssepan.Utility.Mono
{
    /// <summary>
    /// http://www.devexpertise.com/category/net/linq/
    /// </summary>
    public static class LikeOperator
    {
        /// <summary>
        /// Ex: var results = (from v in values where v.Like("*a*a*") select v);
        /// </summary>
        /// <param name="value"></param>
        /// <param name="term"></param>
        /// <returns></returns>
        public static bool Like(this string value, string term)
        {
            bool returnValue = default(bool);
            Regex regex = default(Regex);

            try
            {
                regex = new Regex(string.Format("^{0}$", term.Replace("*", ".*")), RegexOptions.IgnoreCase);
                returnValue = regex.IsMatch(value ?? string.Empty);
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
                throw;
            }
            return returnValue;
        }

        /// <summary>
        /// Ex: var results = values.Like("*a*a*");
        /// </summary>
        /// <param name="source"></param>
        /// <param name="expression"></param>
        /// <returns></returns>
        public static IEnumerable<string> Like(this IEnumerable<string> source, string expression)
        {
            IEnumerable<string> returnValue = default(IEnumerable<string>);

            try
            {
                returnValue = (from s in source where s.Like(expression) select s);
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
                throw;
            }
            return returnValue;
        }
    }
}
