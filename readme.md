# readme.md - README for Ssepan.Utility.Mono (and Ssepan.*.Mono.* Libraries)

## About

Common library of utility functions for C# Mono applications

### Purpose

To encapsulate common functionality, reduce custom coding needed to start a new project, and provide consistency across projects.

### Usage notes

~...

### Setup

TODO

### History

6.2:
~fix error in Log when stackframe has no method info

6.1:
~fix error in log, in building of stack-frame info

6.0:
~initial port from Ssepan.Utility.Core;

### Fixes

~"/usr/lib/mono/xbuild/14.0/bin/Microsoft.Common.targets:  warning : TargetFrameworkVersion 'v4.7' not supported by this toolset (ToolsVersion: 14.0)."
<https://stackoverflow.com/questions/32296141/how-do-i-get-mono-to-use-the-right-toolsversion>

1) change the target framework to "4.5"
2) change Toolsversion from (whatever) to "4.0"

### Known Issues

~  

### Possible Enhancements

~

Steve Sepan
sjsepan@yahoo.com
10/1/2022
